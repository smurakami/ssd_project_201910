import numpy as np
import svgwrite
import re


def create_drawing(size=284, draw_bounds=True):
    dwg = svgwrite.Drawing(size=(size, size))
    if draw_bounds:
        dwg.add(dwg.rect(insert=(0, 0), size=(size, size), stroke_width=2, stroke='black', stroke_dasharray=4, fill='none'))
    return dwg


def get_size_from_drawing(drawing):
    return max(drawing.attribs['width'], drawing.attribs['height'])


def draw_polyline(path, drawing=None, stroke='red', fill='none', stroke_width=1, stroke_linecap='butt'):
    if not drawing:
        drawing = create_drawing()
    size = get_size_from_drawing(drawing)
    drawing.add(drawing.polyline(path * size, stroke=stroke, fill=fill, stroke_width=stroke_width, stroke_linecap=stroke_linecap))
    return drawing


def draw_point(point, drawing=None, r=5, stroke='none', fill='blue'):
    if not drawing:
        drawing = create_drawing()
    size = get_size_from_drawing(drawing)
    drawing.add(drawing.circle(point * size, stroke=stroke, fill=fill, r=r))
    return drawing


def rotationMatrix(degree):
    theta = np.radians(degree)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((c,-s), (s, c)))
    return R


def rotate(vec, deg):
    return np.matmul(vec, rotationMatrix(deg))


def norm(vec):
    x, y = vec
    return np.array([-y, x])


def direction(vec):
    norm = np.linalg.norm(vec)
    if norm > 0:
        return vec / norm
    else:
        return vec * 0.0

get_direction = direction


def get_angle(a, b):
    cos = np.inner(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))
    if np.cross(a, b) >= 0:
        return np.arccos(cos)
    else:
        return -np.arccos(cos)


def smoothclamp(x, mi, mx):
    return mi + (lambda t: np.where(t < 0 , 0, np.where( t <= 1 , 3*t**2-2*t**3, 1 ) ) )( (x-mi)/(mx-mi) )


def get_length(path):
    diff = np.diff(path, axis=0)
    length = np.linalg.norm(diff, axis=1).sum()
    return length



def clothoid(pos, direction=[0, -1], v=0.01, rv=0, drv=6, length=0.5):
    """
    Args:
        pos: start point
        direction: start direction
        v: start velocity
        rv: start angle velocity
        drv: angle accelation
        length: length
    """

    direction = np.array(direction)
    direction = get_direction(direction)
    v_ = v
    v = direction * v

    path = []
    for i in range(int(length/v_)): 
        pos = pos + v
        path.append(pos)
        v = rotate(v, rv)
        rv += drv * v_

    path = np.vstack(path)    
    return path


def clothoid_params_from_edge(edge='bottom'):
    if edge == 'bottom':
        pos = np.array([0.5, 1])
        direction = np.array([0, -1])
    elif edge == 'top':
        pos = np.array([0.5, 0])
        direction = np.array([0, 1])
    elif edge == 'left':
        pos = np.array([0, 0.5])
        direction = np.array([1, 0])
    elif edge == 'right':
        pos = np.array([1, 0.5])
        direction = np.array([-1, 0])
    else:
        raise Exception("please select top, bottom, left, or right")

    return pos, direction


vector_from_edge = clothoid_params_from_edge


def clothoid_from_edge(edge='bottom', v=0.01, rv=0, drv=6, length=0.7):
    pos, direction = clothoid_params_from_edge(edge)
    return clothoid(pos, direction, v, rv, drv, length)


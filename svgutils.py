import numpy as np
import svgwrite
import re
import utils as U


class Path():
    def __init__(self, ops, params):
        self.ops = ops
        self.params = params

    def scale(self, val):
        for param in self.params:
            param *= val


    def translate(self, vec):
        for op, param in zip(self.ops, self.params):
            if op.isupper():
                param += vec

    def rotate(self, degree):
        self.params = [U.rotate(param, degree) for param in self.params]


    @classmethod
    def parse_d(cls, commands):
        ops = []
        params = []

        for command in re.findall('[a-zA-Z][^a-zA-Z]*', commands):
            command = command.strip()
            op = command[0]
            param = command[1:]
            param = param.replace('-', ',-').split(',')
            param = [float(p) for p in param if p != '']
            param = np.array(param).reshape(-1, 2)
            
            ops.append(op)
            params.append(param)

        return ops, params

    @classmethod
    def from_d(cls, commands):
        ops, params = Path.parse_d(commands)
        return Path(ops, params)


    def get_path(self, drawing=None, stroke='none', fill='black', scale=1):
        if not drawing:
            drawing = U.create_drawing()

        path = drawing.path(stroke=stroke, fill=fill)

        for op, param in zip(self.ops, self.params):
            path.push(op, param * scale)

        return path


    def draw(self, drawing=None, stroke='none', fill='black', scale=1):
        if not drawing:
            drawing = U.create_drawing()

        path = self.get_path(drawing, stroke=stroke, fill=fill, scale=scale)
        drawing.add(path)
        return drawing



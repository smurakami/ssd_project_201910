import numpy as np
import svgwrite
import utils as U
from tip import Tip
from svgutils import Path


import xml.etree.ElementTree as ET


def load_path_from_svg():
    tree = ET.parse('./svg/flower.svg')

    root = tree.getroot()
    black, white = tree.findall('.//{http://www.w3.org/2000/svg}path')

    black = Path.from_d(black.attrib['d'])
    white = Path.from_d(white.attrib['d'])

    return black, white


SVG_SIZE = 284    


class Flower():
    def __init__(self, pos, direction, size, parent_pos=None, delta_pos=None):
        black, white = load_path_from_svg()

        for path in [black, white]:
            path.scale(1/SVG_SIZE)
            path.translate(np.array([-0.5, -0.5]))

            angle = U.get_angle(direction, np.array([0, -1])) / np.pi * 180

            path.rotate(angle)
            path.scale(size)
            path.translate(pos)

        self.black = black
        self.white = white

        # for animation
        self.parent_pos = parent_pos
        self.delta_pos = delta_pos

    def draw(self, dwg=None):
        if not dwg:
            dwg = U.create_drawing()

        size = U.get_size_from_drawing(dwg)
        self.black.draw(dwg, fill='black', scale=size)
        self.white.draw(dwg, fill='white', scale=size)

        return dwg



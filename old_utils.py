import aggdraw
from PIL import Image
import numpy as np


def make_norm(p: np.ndarray):
    diff = np.diff(p, axis=0)
    diff = np.vstack([diff, diff[-1:]])
    norm = diff[:, ::-1] * np.array([[1, -1]])
    norm = norm / np.linalg.norm(norm, axis=1)[:, None]
    return norm

makenorm = make_norm    


def ease_inout(length, begin=0, end=1):
    x = np.arange(0, 1, 1/length)
    return begin + ((1 + -np.cos(x * np.pi)) / 2) * (end - begin)


def ease_in(length):
    x = np.linspace(0, 1, length)
    return np.sqrt(1 - x ** 2)


def arc(pos, radius, start=0, end=np.pi*2, length=32):
    x = np.linspace(start, end, length)
    return pos.reshape(-1, 2) + np.vstack([np.cos(x), np.sin(x)]).T * radius


def make_drop(path: np.ndarray, begin, end):
    # begin: 最初の太さ
    # end: 最後の太さ
    p = path
    size = end
    norm = make_norm(p)
    a = norm * ease_inout(len(p), begin, end).reshape(-1, 1)
    angle = np.arctan2(*norm[-1, ::-1])
    b = arc(p[-1], size, angle, angle + np.pi)

    outline = np.vstack([p + a, b, (p - a)[::-1]])    
    return outline


def make_outline(path: np.ndarray, linewidth):
    norm = makenorm(path)

    a = path + norm * linewidth
    b = path - norm * linewidth
    b = b[::-1]

    return np.vstack([a, b])


def concat(a, b):
    return np.vstack([a, b])


def fill(image, path, color):
    w, h = image.size
    brush = aggdraw.Brush(color)
    draw = aggdraw.Draw(image)
    draw.line((path * np.array([[w, h]])).flatten().tolist(), brush)
    draw.flush()


def stroke(image, path, color):
    w, h = image.size
    pen = aggdraw.Pen(color, 1)
    draw = aggdraw.Draw(image)
    draw.line((path * np.array([[w, h]])).flatten().tolist(), pen)
    draw.flush()


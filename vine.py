import numpy as np
import svgwrite
from utils import *
import utils as U
from tip import Tip
from leaf import Leaf
from flower import Flower


DEFAULT_WIDTH = 0.015


class Vine():
    """
    すべての基本となるクラス
    """
    def __init__(self, bone, depth=0):
        self.bone = bone
        self.tip, self.stem = self.make_tip_and_stem()
        self.depth = depth
        self.childs = []
        self.leaves = []
        self.flowers = []
        self.is_pass_through = False

    def make_tip_and_stem(self):
        tip, stem = Tip.split_bone_from_path(self.bone)
        tip = Tip(tip)
        stem = Stem(stem)
        return tip, stem

    def create_leaf(self, parent_pos=0.5, size=0.1, side="left"):
        idx = int(len(self.bone) * parent_pos)

        pos = self.bone[idx]
        diff = np.diff(self.bone, axis=0)
        diff = np.vstack([diff, diff[-1]])
        direction = diff[idx]

        if side == "left":
            angle = 50
        else:
            angle = -50

        direction = U.rotate(direction, angle)
        leaf = Leaf(pos, direction, size, parent_pos=parent_pos, angle=angle)
        self.leaves.append(leaf)

    def create_flower(self, parent_pos=0.5, size=0.1, delta_pos=np.array([0, 0])):
        idx = int(len(self.bone) * parent_pos)

        pos = self.bone[idx]
        pos = pos + delta_pos

        diff = np.diff(self.bone, axis=0)
        diff = np.vstack([diff, diff[-1]])
        direction = diff[idx]

        flower = Flower(pos, direction, size, parent_pos=parent_pos, delta_pos=delta_pos)
        self.flowers.append(flower)

    def draw(self, dwg=None, vine=True, leaf=True, flower=True):
        if not dwg:
            dwg = create_drawing()

        if vine:
            if self.is_pass_through:
                size = U.get_size_from_drawing(dwg)
                draw_polyline(self.bone, dwg, stroke='black', stroke_width=DEFAULT_WIDTH * size, stroke_linecap="round")
            else:
                self.tip.draw(dwg)
                self.stem.draw(dwg)

            for child in self.childs:
                child.draw(dwg, vine=vine, leaf=leaf, flower=flower)

        if leaf:
            for leaf in self.leaves:
                leaf.draw(dwg)

        if flower:
            for flower in self.flowers:
                flower.draw(dwg)

        return dwg


    # def pass_to_edge(self, edge='left'):
    #     pos, direction = U.vector_from_edge('right')
    #     target_dir = -direction

    #     speed = 0.01
    #     angle_speed = 8.0

    #     p = self.bone[-1]
    #     d = np.diff(self.bone, axis=0)[-1]

    #     v = U.get_direction(d) * speed
    #     angle = U.get_angle(d, target_dir) * 180 / np.pi

    #     # 方向転換
    #     dtheta = angle_speed * np.sign(angle)
    #     path = []
    #     for _ in range(int(np.abs(angle/dtheta))):
    #         v = U.rotate(v, -dtheta)
    #         p = p + v
    #         path.append(p)
            
    #     # スムージングを活用しつつ抜ける
    #     u = target_dir * speed
    #     v = U.norm(u)

    #     dist = pos - p
    #     steps = np.abs(np.inner(u, dist) / np.linalg.norm(u) ** 2)
    #     steps = int(steps) + 1
    #     vscale = np.inner(v, dist) / np.linalg.norm(v) ** 2

    #     for i in range(steps):            
    #         path.append(p + u * i + v * vscale * U.smoothclamp(i, 0, steps-1))
            
    #     path = np.array(path)

    #     # tipを取り除く信号

    #     self.bone = np.vstack([self.bone, path])
    #     self.is_pass_through = True


class ClothoidVine(Vine):
    @classmethod
    def from_edge(cls, edge, v=0.01, rv=0, drv=6, length=0.5):
        pos, direction = U.clothoid_params_from_edge(edge)
        return ClothoidVine(pos, direction, v, rv, drv, length)

    def __init__(self, pos, direction, v=0.01, rv=0, drv=6, length=0.5, depth=0, parent_pos=None, scale=None):
        bone = clothoid(pos, direction, v, rv, drv, length)
        super().__init__(bone, depth)
        self.pos = pos
        self.direction = direction
        self.v = v
        self.rv = rv
        self.drv = drv
        self.length = length
        self.parent_pos = parent_pos
        self.scale = scale

    def create_child(self, pos=0.3, scale=0.3):
        parent = self.bone

        parent_pos = pos
        idx = int(len(parent) * pos)

        v = self.v
        rv = self.rv
        drv = self.drv
        length = self.length

        pos = parent[idx]
        direction = np.diff(parent, axis=0)[idx]

        child = ClothoidVine(
            pos,
            direction=direction,
            v=v * scale,
            rv=rv,
            drv=-drv / scale,
            length=length * scale,
            depth=self.depth + 1,
            parent_pos=parent_pos,
            scale=scale)
        self.childs.append(child)
        return child


    def animate(self, t):
        animated = ClothoidVine(
            self.pos,
            self.direction,
            self.v,
            self.rv * t,
            self.drv * t,
            self.length * t,
            self.depth )

        for child in self.childs:
            animated.create_child(pos=child.parent_pos, scale=child.scale)

        animated.childs = [child.animate(t) for child in animated.childs]

        return animated



class Stem():
    """
    茎の描画関連
    """
    def __init__(self, bone):
        self.bone = bone

    def draw(self, dwg=None):
        if not dwg:
            dwg = create_drawing()
        size = get_size_from_drawing(dwg)
        dwg = draw_polyline(self.bone, dwg, stroke='black', stroke_width=DEFAULT_WIDTH * size, stroke_linecap="round")
        return dwg

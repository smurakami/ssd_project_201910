import numpy as np
import svgwrite
import utils as U
from tip import Tip
from svgutils import Path


import xml.etree.ElementTree as ET


def load_path_from_svg():
    tree = ET.parse('./svg/leaf.svg')
    path = tree.find('.//{http://www.w3.org/2000/svg}path')
    commands = path.attrib['d']

    return commands


SVG_SIZE = 284    


class Leaf():
    def __init__(self, pos, direction, size, parent_pos=None, angle=None):
        commands = load_path_from_svg()
        path = Path.from_d(commands)

        path.scale(1/SVG_SIZE)
        path.translate(np.array([-0.5, -1]))

        angle = U.get_angle(direction, np.array([0, -1])) / np.pi * 180

        path.rotate(angle)
        path.scale(size)
        path.translate(pos)

        self.path = path

        # for animation
        self.parent_pos = parent_pos
        self.angle = angle

    def draw(self, dwg=None):
        if not dwg:
            dwg = U.create_drawing()

        size = U.get_size_from_drawing(dwg)
        self.path.draw(dwg, scale=size)

        return dwg



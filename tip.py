import numpy as np
import svgwrite
from utils import *


DEFAULT_WIDTH = 0.015


class Tip():
    def __init__(self, bone, drawing=None, width=DEFAULT_WIDTH):
        if not drawing:
            drawing = create_drawing()
        size = drawing.attribs['width']
        self.drawing = drawing
        self.bone = bone
        self.size = size
        self.width = width
        self.construct(width)

    @classmethod
    def get_bone_from_path(cls, path, width=DEFAULT_WIDTH):
        # length = get_length(path)
        target_length = width / 0.12
        for i in range(len(path)):
            length = get_length(path[i:])
            if length < target_length:
                return path[i:]

        raise ValueError("something wrong...!")


    @classmethod
    def split_bone_from_path(cls, path, width=DEFAULT_WIDTH):
        # length = get_length(path)
        target_length = width / 0.12
        for i in range(len(path)):
            length = get_length(path[i:])
            if length < target_length:
                return path[i:], path[:i + 2]

        raise ValueError("something wrong...!")

        # assert "something wrong...!"
        # print('something wrong...!')
        # return path


    def construct(
        self,
        width=None,
        begin_anchor_rate=0.35,
        bulge_anchor_rate_backward=0.25,
        bulge_anchor_rate_forward=0.12,
        bulge_pos=0.75,
        bulge_size=0.20,
        end_anchor_rate_to_bulge=0.26):

        size = self.size
        bone = self.bone

        if not width:
            width = self.width

        diff = np.diff(bone, axis=0)
        diff = np.vstack([diff, diff[-1:]])
        length = np.linalg.norm(diff, axis=1).sum()

        # begin point
        # width = length * 0.05
        idx = 0
        dir = direction(diff[idx])
        norm_direction = norm(dir)

        p = bone[idx] + width * norm_direction * 0.5
        a = p + dir * length * begin_anchor_rate

        begin_point = {
            "pos": p,
            "a": a,    
        }

        # end point
        p = bone[idx] - width * norm_direction * 0.5
        a_ = p + dir * length * begin_anchor_rate

        end_point = {
            "pos": p,
            "a_": a_
        }

        # second point

        s = length * bulge_size
        idx = int(len(bone) * bulge_pos)
        dir = direction(diff[idx])
        norm_direction = norm(dir)

        p = bone[idx] + norm_direction * s
        a_ = p - dir * length * bulge_anchor_rate_backward
        a = p + dir * length * bulge_anchor_rate_forward

        second_point = {
            "pos": p,
            "a": a,
            "a_": a_
        }

        # 4th point
        p = bone[idx] - norm_direction * s
        a_ = p + dir * length * bulge_anchor_rate_forward
        a = p - dir * length * bulge_anchor_rate_backward

        forth_point = {
            "pos": p,
            "a": a,
            "a_": a_
            }

        # third point
        idx = len(bone) - 1
        p = bone[idx]

        dir = direction(diff[idx])
        norm_direction = norm(dir)

        a = p - norm_direction * s * end_anchor_rate_to_bulge
        a_ = p + norm_direction * s * end_anchor_rate_to_bulge

        third_point = {
            "pos": p,
            "a": a,
            "a_": a_
        }

        points = [
            begin_point,
            second_point,
            third_point,
            forth_point,
            end_point,
        ]

        self.points = points

    def draw(self, drawing=None, stroke='none', fill='black'):
        if not drawing:
            drawing = self.drawing

        d = drawing
        size = self.size
        points = self.points

        begin_point = points[0]
        path = d.path(stroke_width=1, stroke=stroke, fill=fill)
        path.push('M', begin_point['pos'] * size)

        for i in range(1, len(points)):    
            path.push('C', np.array([
                points[i-1]['a'],
                points[i]['a_'],
                points[i]['pos']
            ]) * size)

        d.add(path)
        return d

    def draw_keypoints(self, drawing=None):
        if not drawing:
            drawing = self.drawing

        d = drawing
        size = self.size
        points = self.points

        for point in points:
            d.add(d.circle(point['pos'] * size, fill='red', r=5))
            if "a" in point:
                p = point['pos']
                a = point["a"]
                d.add(d.line(start=p * size, end=a*size, stroke_width=1, stroke='green'))
            if "a_" in point:
                p = point['pos']
                a_ = point["a_"]
                d.add(d.line(start=p * size, end=a_*size, stroke_width=1, stroke='green'))

        d.add(d.polyline(points=self.bone * size, stroke_width=1, stroke='blue', fill='none'))

        return d


